package com.mercesletifer.util;

import com.mercesletifer.common.InitializationException;
import com.mercesletifer.domain.StockSymbol;
import com.mercesletifer.domain.StockTick;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author Michael McConnell
 * @since 20161130
 */
public class ExportHistoricalData {
    static final EMACalculator EMA_CALC = new EMACalculator();
    static final String ExportCSVPath = "/home/michael/Code/reactive_programming/data/export";
    static final String ImportCSVPath = "/home/michael/Code/reactive_programming/data/import";
    static final DateTimeFormatter ISO_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    static final Logger LOG = LoggerFactory.getLogger(ExportHistoricalData.class);
    static final String StockSymbolCSVHeader = "id|symbol";
    static final String TradeCVSHeader = "id|date|userId|numShares|price|stockTickId|tradeType";
    static final String TradeDateCVSHeader = "id|date";
    static int EMA13Window = 13;
    static int EMA26Window = 26;
    static List<File> fileList;
    static List<StockSymbol> StockSymbolList;
    static Map<LocalDate, com.mercesletifer.domain.TickDate> LocalDate2TickDateMap;
    static Map<String, List<String>> NameToCSVListMap;
    static File StockDir = null;
    static Map<StockSymbol, List<StockTick>> Symbol2TickMap;
    static List<StockSymbol> SymbolList;
    private static Map<String, File> Name2CSVFileMap;

    public ExportHistoricalData() {
    }

    public static void main(String argv[]) throws Exception {
        ExportHistoricalData suac = new ExportHistoricalData();
        suac.createStockTickExportCSV();
        // for (String date : GetAllTickDates()) {
        // System.out.println("date: " + date);
        // }
    }

    public void createNeo4JImportCsv() throws Exception {
//        createStockSymbolExportCSV();
        createStockTickExportCSV();
//        createTickDateExportCSV();
//        createUserAccountNeo4JImportCsv();
    }

    public void createStockSymbolExportCSV() throws IOException {
        File stocksCSV = null;
        stocksCSV = new File(ExportCSVPath + "/stocks.csv");

        try (PrintWriter csvWriter = new PrintWriter(new BufferedWriter(new FileWriter(stocksCSV)))) {
            List<StockSymbol> symbolList = getStockTicks();
            int symCount = 0;
            csvWriter.println(StockSymbolCSVHeader);
            for (StockSymbol symbol : symbolList) {
                csvWriter.println("  " + symCount++ + " | " + symbol.getSymbol());
            }
        }


    }

    public void createStockTickExportCSV() throws IOException {
        String TickDateCSVHeader = "date|symbol|price|volume|ema13|ema26";
        if (SymbolList == null || SymbolList.isEmpty()) {
            SymbolList = getStockTicks();
        }


        for (StockSymbol symbol : SymbolList) {
            File tickOPFile = new File(ExportCSVPath + "/" + symbol.getSymbol() + ".csv");
            if (tickOPFile instanceof File) {
                try (PrintWriter printer = new PrintWriter(new BufferedWriter(new FileWriter(tickOPFile)))) {
                    printer.println(TickDateCSVHeader);
                    for (StockTick st : symbol.getStockTicks()) {
                        printer.println(st.getDate() + "|" + st.getStockSymbol().getSymbol() + "|" + st.getPrice() + "|" + st.getVolume() + "|" + st.getEma13() + "|" + st.getEma26());
                    }
                }
            } else {
                throw new InitializationException("file: " + tickOPFile.getCanonicalPath() + " cannot be created or opened!");
            }
        }
    }

    public void createTickDateExportCSV() throws IOException {
        File tickDateCSV = new File(ExportCSVPath + "/tickDate.csv");
        Map<LocalDate, com.mercesletifer.domain.TickDate> tickDateMap = getLocalDate2TickDateMap();
        Set<com.mercesletifer.domain.TickDate> tickDates = new HashSet<>();
        com.mercesletifer.domain.TickDate[] sortedAscending = new com.mercesletifer.domain.TickDate[tickDateMap.size()];
        int inx = 0;
        Set<Map.Entry<LocalDate, com.mercesletifer.domain.TickDate>> mapentrySet = tickDateMap.entrySet();
        Set<LocalDate> localDateSet = new HashSet<>();
        for (Map.Entry<LocalDate, com.mercesletifer.domain.TickDate> entry : mapentrySet) {
            localDateSet.add(entry.getKey());
        }
        for (LocalDate ld : localDateSet) {
            sortedAscending[inx++] = tickDateMap.get(ld);
        }
        Arrays.sort(sortedAscending, (com.mercesletifer.domain.TickDate d1, com.mercesletifer.domain.TickDate d2) -> d1.compareTo(d2));
        try (PrintWriter csvWriter = new PrintWriter(new BufferedWriter(new FileWriter(tickDateCSV)))) {
            for (com.mercesletifer.domain.TickDate td : sortedAscending) {
                Collection<StockTick> stockTicks = td.getTicksForDate();
                for (StockTick stockTick : stockTicks) {
                    // date|id|symbol|stockId|open|high|low|close|volume|ema13|ema26
                    csvWriter.println(td.getDate() + "|" + td.getId() + "|" + stockTick.getStockSymbol().getSymbol()
                            + "|" + stockTick.getId() + "|" + stockTick.getPrice() + "|" + stockTick.getVolume() + "|"
                            + stockTick.getEma13() + "|" + stockTick.getEma26());
                }
            }
        }
    }

    public void createUserAccountNeo4JImportCsv() {

    }

    public List<StockSymbol> getStockTicks() throws NumberFormatException, IOException {
        if (SymbolList instanceof List && !SymbolList.isEmpty()) {
            return SymbolList;
        }
        SymbolList = new ArrayList<>();
        StockSymbol sSymbol = null;
        for (String name : getName2CSVListMap().keySet()) {
            sSymbol = new StockSymbol(name);
            int lineCount = 0;
            String[] previousDayArray = new String[]{"",  "0", "0"};
            List<String> csvList = getName2CSVListMap().get(name);
            for (String csValues : csvList) {
                if (lineCount++ == 0)
                    continue;
                LOG.debug("stock symbol: {}, csv string: {}", name, csValues);
                String[] tokenArray = csValues.split(",");
                if (tokenArray.length != 3) {
                    StringBuilder sb = new StringBuilder();
                    int currTok = 0;
                    for (String tok : tokenArray) {
                        sb.append("tok[").append(currTok++).append("] = ").append(tok)
                                .append(currTok < tokenArray.length - 1 ? "," : "");
                    }
                    LOG.error("invalid token array for " + name + ", length = " + tokenArray.length + " token array: "
                            + sb.toString());
                    continue;
                }

                com.mercesletifer.domain.TickDate tickDate = new com.mercesletifer.domain.TickDate(tokenArray[0]);
                if (getLocalDate2TickDateMap().get(tickDate.getTickDateLD()) != null) {
                    tickDate = getLocalDate2TickDateMap().get(tickDate.getTickDateLD());
                } else {
                    getLocalDate2TickDateMap().put(tickDate.getTickDateLD(), tickDate);
                }
                StockTick tick = new StockTick(sSymbol, tickDate);

                try {
                    LOG.debug(" price = {}", tokenArray[1]);
                    if (tokenArray[1].equals(("-"))) {
                        tokenArray[1] = previousDayArray[1];
                    }
                    tick.setPrice(Double.valueOf(tokenArray[1]));
                } catch (NumberFormatException nfe) {
                    LOG.error("error with stock: {}, price {}", sSymbol.getSymbol(), tokenArray[1], nfe);
                    continue;
                }
                // Volume
                LOG.debug(" volume = {}", tokenArray[2]);
                if (tokenArray[2].equals("-")) {
                    tokenArray[2] = previousDayArray[2];
                }
                tick.setVolume(Integer.valueOf(tokenArray[2]));
                previousDayArray = tokenArray;
            }
            SymbolList.add(sSymbol);
        }
        EMACalculator emaCalc = new EMACalculator();
        for (StockSymbol symbol : SymbolList) {
            List<StockTick> tickList = null;
            tickList = emaCalc.calculateMACD(symbol.getStockTicks());
            symbol.setStockTicks(tickList);
        }

        return SymbolList;
    }

    private String[] getAllTickDates() {

        List<String> localDateSortedList = new ArrayList<>();
        Map<LocalDate, com.mercesletifer.domain.TickDate> tickDateMap = getLocalDate2TickDateMap();
        Set<LocalDate> localDates = tickDateMap.keySet();
        String[] dateArray = new String[localDates.size()];

        int inx = 0;
        for (LocalDate localDate : localDates) {
            dateArray[inx++] = ISO_FORMATTER.format(localDate);
        }
        Arrays.sort(dateArray, (String s1, String s2) -> s1.compareTo(s2));
        return dateArray;
    }

    private Map<LocalDate, com.mercesletifer.domain.TickDate> getLocalDate2TickDateMap() {
        if (LocalDate2TickDateMap == null)
            LocalDate2TickDateMap = new HashMap<>();
        return LocalDate2TickDateMap;
    }

    private Map<String, File> getName2CSVFileMap() {
        if (Name2CSVFileMap == null) {
            Name2CSVFileMap = initializeInput();
        }
        return Name2CSVFileMap;
    }

    private Map<String, List<String>> getName2CSVListMap() throws IOException {
        List<String> csvlist = null;
        if (NameToCSVListMap instanceof Map) {
            return NameToCSVListMap;
        }
        NameToCSVListMap = new HashMap<>();
        Set<String> fileNames = getName2CSVFileMap().keySet();
        for (String fName : fileNames) {
            File csvFile = getName2CSVFileMap().get(fName);
            try (BufferedReader bReader = new BufferedReader(new FileReader(csvFile))) {
                String buff = null;
                csvlist = new ArrayList<>();
                while ((buff = bReader.readLine()) != null) {
                    csvlist.add(buff);
                }
            }
            NameToCSVListMap.put(fName, csvlist);
        }
        return NameToCSVListMap;
    }

    private Map<String, File> initializeInput() {
        Map<String, File> name2CSVFileMap = new HashMap<>();
        try {
            StockDir = new File(ImportCSVPath);
            if (StockDir.exists()) {
                String[] stockFiles = StockDir.list();
                for (String fname : stockFiles) {
                    File csvFile = new File(ImportCSVPath + "/" + fname);
                    String symName = csvFile.getName();
                    int start, end;
                    start = 0;
                    end = symName.indexOf(".csv");
                    CharSequence symCharSeq = symName.subSequence(start, end);
                    if (csvFile.exists() && csvFile.getCanonicalPath().endsWith(".csv")) {
                        name2CSVFileMap.put(symCharSeq.toString(), csvFile);
                        LOG.debug("name2CSVFileMap adding symbol: {}, with file: {}", symCharSeq.toString(),
                                csvFile.getCanonicalPath());
                    }
                }
            }
        } catch (Throwable t) {
            LOG.error("unexpected error when reading csv file", t);
        }
        return name2CSVFileMap;
    }

}
