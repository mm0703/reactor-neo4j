package com.mercesletifer.repository;

import com.mercesletifer.domain.StockTick;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;

/**
 * @author Michael McConnell
 * @since 20161201
 */
@Repository
public interface TickDateRepository extends GraphRepository<com.mercesletifer.domain.TickDate> {

    com.mercesletifer.domain.TickDate findOne(LocalDate dateValue);

    Collection<StockTick> findAll(com.mercesletifer.domain.TickDate tickDateValue);
}
