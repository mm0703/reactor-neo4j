package com.mercesletifer.repository;

import com.mercesletifer.domain.StockSymbol;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author Michael McConnell
 * @since 20161119
 */
@Repository
public interface StockRepository extends GraphRepository<StockSymbol> {

    StockSymbol findOne(String symbol);

    StockSymbol findOne(Long id);

    List<StockSymbol> findAll();

    List<com.mercesletifer.domain.StockTick> findAll(StockSymbol stockSymbolValue);

    Set<com.mercesletifer.domain.Trade> findAllTrades(StockSymbol stockSymbolValue);
}
