package com.mercesletifer.repository;


import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;

/**
 * @author Michael McConnell
 * @since 20161119
 */
@Repository
public interface TradeDateRepository extends GraphRepository<com.mercesletifer.domain.TradeDate> {


    // derived finder
    com.mercesletifer.domain.TradeDate findOne(LocalDate date);

    com.mercesletifer.domain.TradeDate findOne(String date);

    com.mercesletifer.domain.TradeDate findOne(Long id);

    Collection<com.mercesletifer.domain.TradeDate> findAll();

    com.mercesletifer.domain.TradeDate addTrade(com.mercesletifer.domain.Trade tradeValue);


}
