package com.mercesletifer.repository;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * @author Michael McConnell
 * @since 20161121
 */
@Repository
public interface UserAccountRepository extends GraphRepository<com.mercesletifer.domain.UserAccount> {

    com.mercesletifer.domain.UserAccount findOne(Long id);

    com.mercesletifer.domain.UserAccount findByName(String name);

    Collection<com.mercesletifer.domain.UserAccount> findAll();

    Collection<com.mercesletifer.domain.Trade> findAllTrades(com.mercesletifer.domain.UserAccount accnt);

}
