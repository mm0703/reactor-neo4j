package com.mercesletifer;

import com.mercesletifer.domain.StockSymbol;
import com.mercesletifer.repository.StockRepository;
import com.mercesletifer.repository.TickDateRepository;
import com.mercesletifer.util.LoadAndCalculate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@SpringBootApplication
@EnableNeo4jRepositories
public class Application {
    static final Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner demo(StockRepository stockRepositoryValue, TickDateRepository dateRepositoryValue) {
        return args -> {

            stockRepositoryValue.deleteAll();

            LOG.info("Before linking up with Neo4j...");

            List<StockSymbol> stockList = LoadAndCalculate.GetStockTicks();

            for (StockSymbol symbol : stockList) {
                stockRepositoryValue.save(symbol);
            }

            List<LocalDate> dates = new ArrayList<>();
            LOG.info("Lookup each stock by symbol...");
            stockList.stream().forEach(stock ->
                                               LOG.info(
                                                       "\t" + stockRepositoryValue.findOne(stock.getSymbol()).toString())
            );
        };
    }


}
