package com.mercesletifer.domain;

import com.mercesletifer.common.InitializationException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Relationship;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

/**
 * @author Michael McConnell
 * @since 20161201
 */
public class TickDate implements Comparable, Serializable{

    static final DateTimeFormatter D_FORMATTER1 = DateTimeFormatter.ofPattern("dd-MMM-yy");
    static final DateTimeFormatter D_FORMATTER2 = DateTimeFormatter.ofPattern("d-MMM-yy");
    static final DateTimeFormatter ISO_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");
    static final DateTimeFormatter ISO_FORMATTER1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    static final DateTimeFormatter ISO_FORMATTER2 = DateTimeFormatter.ofPattern("yyyy-MM-d");
    static final Logger LOG = LoggerFactory.getLogger(StockTick.class);
    private static final long serialVersionUID = -4270123443713406628L;

    static {
        D_FORMATTER1.withLocale(Locale.US);
        D_FORMATTER2.withLocale(Locale.US);
    }

    @GraphId
    Long id;
    LocalDate tickDate;
    @Relationship(type = "TICKS", direction = Relationship.UNDIRECTED)
    Collection<StockTick> ticksForDate;
    
    public TickDate(String date) {
        setDate(date);
    }

    public Long getId() {
        return id;
    }
    @NotNull
    public void setId( final Long idValue) {
        id = idValue;
    }

    public String getDate() {
        return ISO_FORMATTER.format(getTickDateLD());
    }
    @NotNull
    public void setDate( final String tickDateValue) {
        try {
            tickDate = LocalDate.parse(tickDateValue, D_FORMATTER1);
        } catch (DateTimeParseException dtpe) {
            try {
                tickDate = LocalDate.parse(tickDateValue, D_FORMATTER2);
            } catch (DateTimeParseException dtpe1) {
                try {
                    tickDate = LocalDate.parse(tickDateValue, ISO_FORMATTER1);
                } catch (DateTimeParseException dtpe2) {
                    try {
                        tickDate = LocalDate.parse(tickDateValue, ISO_FORMATTER2);
                    } catch (DateTimeParseException dtpe3) {
                        throw new InitializationException("error(s) occurred attempting to construct tickDate", dtpe3);
                    }
                }
            }
        }
    }

    public LocalDate getTickDateLD() {
        if (tickDate == null) throw new IllegalStateException("This tickDate is in an invalid state: No LocalDate provided!");
        return tickDate;
    }

    public Collection<StockTick> getTicksForDate() {
        if (ticksForDate == null) ticksForDate = new ArrayList<>();
        return ticksForDate;
    }

    public int hashCode() {
        HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
        hashCodeBuilder.append(this.tickDate);
        return hashCodeBuilder.toHashCode();
    }

    public boolean equals(Object other) {
        if (other != this) {
            if (other instanceof TickDate) {
                TickDate that = (TickDate) other;
                EqualsBuilder equalsBuilder = new EqualsBuilder();
                equalsBuilder.append(this.getTickDateLD(), that.getTickDateLD());
                return equalsBuilder.isEquals();
            }
            return false;
        }
        return true;
    }

	@Override
	public int compareTo(Object o) {
		if (o instanceof TickDate){
			TickDate other = (TickDate)o;
			if (this.getDate() != null && other.getDate() != null){
				return this.getDate().compareTo(other.getDate());
			}
		}
		return 1;
	}
}
