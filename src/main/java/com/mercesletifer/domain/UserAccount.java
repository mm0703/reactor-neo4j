package com.mercesletifer.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author Michael McConnell
 * @since 20161121
 */
@NodeEntity
public class UserAccount implements Serializable{

    private static final long serialVersionUID = -8902974352137057661L;
    @NotNull
    @GraphId
    Long id;
    @NotNull
    String name;
    @NotNull
    @Relationship(type = "TRADES", direction = Relationship.UNDIRECTED)
    Collection<Trade> trades;

    @NotNull
    BigDecimal accountBalance;
    public UserAccount(String name){
        setName(name);
    }
    public Long getId() {
        return id;
    }

    public void setId(final Long idValue) {
        id = idValue;
    }

    public String getName() {
        return name;
    }

    public void setName(final String nameValue) {
        name = nameValue;
    }

    public Collection<Trade> getTrades() {
        if (trades == null) trades = new HashSet<>();
        return trades;
    }

    public void setTrades(final Collection<Trade> tradesValue) {
        trades = tradesValue;
    }

    public BigDecimal getAccountBalance() {
        if (accountBalance == null)
            accountBalance = new BigDecimal(100000.0, MathContext.DECIMAL32).setScale(2, RoundingMode.HALF_EVEN);
        return accountBalance;
    }

    public void setAccountBalance(final BigDecimal accountBalanceValue) {
        accountBalance = accountBalanceValue;
    }

    public void addTrade(Trade tradeValue) {
        getTrades().add(tradeValue);
        updateAccountBalance(tradeValue);
    }

    private void updateAccountBalance(Trade tradeValue) throws RuntimeException {

        switch (tradeValue.getTradeType()) {
            case BOUGHT_TO_OPEN: {
                BigDecimal cost
                        = new BigDecimal(-1.0 * tradeValue.getAmount() + tradeValue.getFee(), MathContext.DECIMAL32).setScale(2, BigDecimal.ROUND_HALF_EVEN);
                BigDecimal accBal = getAccountBalance();
                accBal.add(cost);
                setAccountBalance(accBal);
                addTrade(tradeValue);
                break;
            }
            case SOLD_TO_OPEN: {
                BigDecimal cost
                        = new BigDecimal((-1.0 * tradeValue.getAmount() +tradeValue.getFee()), MathContext.DECIMAL32).setScale(2, RoundingMode.HALF_EVEN);
                BigDecimal accBal = getAccountBalance();
                accBal.add(cost);
                addTrade(tradeValue);
                break;
            }
            case BOUGHT_TO_CLOSE: {
                BigDecimal cost
                        = new BigDecimal((tradeValue.getAmount() + tradeValue.getFee()), MathContext.DECIMAL32).setScale(2, RoundingMode.HALF_EVEN);
                BigDecimal accBal = getAccountBalance();
                accBal.add(cost);
                addTrade(tradeValue);
                break;
            }
            case SOLD_TO_CLOSE: {
                BigDecimal cost
                        = new BigDecimal((tradeValue.getAmount() + tradeValue.getFee()), MathContext.DECIMAL32).setScale(2, RoundingMode.HALF_EVEN);
                BigDecimal accBal = getAccountBalance();
                accBal.add(cost);
                addTrade(tradeValue);
                break;
            }
            default: {
                throw new RuntimeException("Illegal Trade Type");
            }
        }
    }

}
