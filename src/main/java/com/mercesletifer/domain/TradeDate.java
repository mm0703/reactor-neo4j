package com.mercesletifer.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Michael McConnell
 * @since 20161119
 */
@NodeEntity
public class TradeDate implements Serializable {
    private static final long serialVersionUID = -968973529987852672L;
    @GraphId
    @NotNull
    Long id;
	
    @NotNull
    LocalDate tradeDate;
    @NotNull
    @Relationship(type = "TRADES_THIS_DATE", direction = Relationship.UNDIRECTED)
    Set<Trade> trades;

    public TradeDate(LocalDate tradeDateValue) {
        setTradeDate(tradeDateValue);
    }
    public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}
    public LocalDate getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(final LocalDate tradeDateValue) {
        tradeDate = tradeDateValue;
    }

    public boolean addTrade(Trade tradeValue) {
        return getTrades().add(tradeValue);
    }

    public Set<Trade> getTrades() {
        if (trades == null) trades = new HashSet<>();
        return trades;
    }

}
