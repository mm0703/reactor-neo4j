package com.mercesletifer.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * @author Michael McConnell
 * @since 20161121
 */
@NodeEntity
public class Trade {
    public enum TradeType {
        BOUGHT_TO_CLOSE, BOUGHT_TO_OPEN, SOLD_TO_CLOSE, SOLD_TO_OPEN
    }
    @Relationship(type = "USER_ACCOUNT", direction = Relationship.UNDIRECTED)
    UserAccount account;
    ZonedDateTime dateTimeStamp;
    @Relationship(type = "TRADE_DATE", direction = Relationship.UNDIRECTED)
    TradeDate dateTraded;
    
    @GraphId
    Long id;
    Integer numShares;
    BigDecimal price;
    @Relationship(type = "SYMBOL_TRADED", direction = Relationship.UNDIRECTED)
    StockSymbol stockSymbol;
    @Relationship(type = "TRADE", direction = Relationship.UNDIRECTED)
    StockTick stockTick;
    TradeType tradeType;

    UserAccount user;

    public Trade() {
        setTradeTime();
    }

    public Trade(TradeDate tradeDateValue){
        this();
        setDateTraded(tradeDateValue);
    }

    public Trade(TradeDate tradeDateValue, UserAccount userValue, TradeType tradeTypeValue, StockSymbol stockSymbolValue, StockTick stockTickValue, Double priceValue) {
        this(tradeDateValue);
        setUser(userValue);
        setTradeType(tradeTypeValue);
        setSymbol(stockSymbolValue);
        setStockTick(stockTickValue);
        setPrice(priceValue);
    }

    public boolean equals(Object other) {

        if (other instanceof Trade) {
            if (other != this) {
                Trade that = (Trade) other;
                EqualsBuilder equalsBuilder = new EqualsBuilder();
                equalsBuilder.append(this.account, that.account)
                        .append(this.stockTick, that.stockTick)
                        .append(this.price, that.price)
                        .append(this.tradeType, that.tradeType)
                        .append(this.dateTimeStamp, that.dateTimeStamp)
                        .append(this.dateTraded, that.dateTraded);
                return equalsBuilder.isEquals();
            }
            return true;
        }
        return false;
    }

    public UserAccount getAccount() {
        return account;
    }
    public Double getAmount() {
    	Double tmpAmt = getPrice() * getNumShares();
    	BigDecimal bd = new BigDecimal(tmpAmt, MathContext.DECIMAL32);
    	bd.setScale(2, RoundingMode.HALF_EVEN);
    	return bd.doubleValue();
    }

    public TradeDate getDateTraded() {
        return dateTraded;
    }

    public Double getFee() {
		return 15.00 + getNumShares() * 0.1;
	}

    public Long getId() {
        return id;
    }

	public Integer getNumShares() {
        return numShares;
    }

	public Double getPrice() {
        return price.doubleValue();
    }
    public StockTick getStockTick() {
        return stockTick;
    }

    public StockSymbol getSymbol() {
        return stockSymbol;
    }
    public ZonedDateTime getTradeTime() {
        return dateTimeStamp;
    }

    public TradeType getTradeType() {
        return tradeType;
    }
    public UserAccount getUser() {
        return user;
    }

    public int hashCode() {
        HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
        hashCodeBuilder.append(this.account)
                .append(this.stockTick)
                .append(this.price)
                .append(this.tradeType)
                .append(this.dateTimeStamp)
                .append(this.dateTraded);
        return hashCodeBuilder.toHashCode();
    }
    @NotNull
    public void setAccount(final UserAccount accountValue) {
        account = accountValue;
    }

    @NotNull
    public void setDateTraded(final TradeDate dateTradedValue) {
        dateTraded = dateTradedValue;
    }
    
    public void setId(final Long idValue) {
        id = idValue;
    }
    
    @Size( min = 1 )
    public void setNumShares(final Integer numSharesValue) {
        numShares = numSharesValue;
    }

    private void setPrice(BigDecimal priceValue) {
        price = priceValue;
    }

    @NotNull
    public void setPrice(final Double priceValue) {
        BigDecimal bigDecimal = new BigDecimal(priceValue, MathContext.DECIMAL32);
        bigDecimal.setScale(2, RoundingMode.HALF_EVEN);
        setPrice(bigDecimal);
    }

    @NotNull
    public void setStockTick(final StockTick stockTickValue) {
        stockTick = stockTickValue;
    }
    @NotNull
    public void setSymbol(final StockSymbol symNameValue) {
        stockSymbol = symNameValue;
    }

    public void setTradeTime() {
        ZoneId zoneId = ZoneId.of("UTC");
        dateTimeStamp = ZonedDateTime.now().withZoneSameInstant(zoneId);
    }


    @NotNull
    public void setTradeType(final TradeType tradeTypeValue) {
        tradeType = tradeTypeValue;
    }

    @NotNull
    public void setUser(final UserAccount userValue) {
        user = userValue;
    }

}
