package com.mercesletifer.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;


/**
 * @author Michael McConnell
 * @since 20161119
 */
@NodeEntity
public class StockSymbol implements Serializable {
    @NotNull
    @GraphId Long id;
    @NotNull
    @Relationship(type = "STOCK_SYMBOL", direction = Relationship.UNDIRECTED)
    List<StockTick> stockTicks;
    @NotNull
    @Relationship(type = "TRADE_SYMBOL", direction = Relationship.UNDIRECTED)
    Collection<Trade> trades;

    @NotNull String symbol;

    public StockSymbol() {
    }

    public StockSymbol(String name) {
        setSymbol(name);
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long idValue) {
        id = idValue;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(final String symbolValue) {
        symbol = symbolValue;
    }

    public List<StockTick> getStockTicks() {
        if (stockTicks == null) stockTicks = new ArrayList<>();
        return stockTicks;
    }
    public void addStockTick(StockTick st){
    	getStockTicks().add(st);
    }

    public StockSymbol findOne(String s){
        if (s != null && !s.equals("") && s.equals(this.getSymbol())){
            return this;
        }
        return null;
    }


    public boolean addTrade(Trade tradeValue) {
        return getTrades().add(tradeValue);
    }

    public void addTick(StockTick t){
        getStockTicks().add(t);
    }


    public Collection<Trade> getTrades() {
        if (trades == null) trades = new HashSet<>();
        return trades;
    }
}
