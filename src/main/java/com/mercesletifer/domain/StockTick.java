package com.mercesletifer.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;


/**
 * @author Michael McConnell
 * @since 20161119
 * Represents a stock and a price at a specified time.
 */
@NodeEntity
public class StockTick implements Comparable<LocalDate>, Serializable {

    static final Logger LOG = LoggerFactory.getLogger(StockTick.class);
    private static final long serialVersionUID = 4123092334061001264L;
    @NotNull
    @GraphId
    Long id;
    @Relationship(type = "TICK_DATE", direction = Relationship.UNDIRECTED)
    TickDate tickDate;
    @NotNull
    @Relationship(type = "SYMBOL", direction = Relationship.UNDIRECTED)
    private StockSymbol stockSymbol;

    private int volume;
    private BigDecimal price;
    private BigDecimal ema13;
    private BigDecimal ema26;



    public StockTick() {
    }

    public StockTick(StockSymbol s) {
        this();
        setStockSymbol(s);
    }

    public StockTick(StockSymbol stockSymbolValue, TickDate tickDateValue) {
        this(stockSymbolValue);
        setTickDate(tickDateValue);
    }

    public StockTick(StockSymbol stockSymbolValue, TickDate tickDateValue, Double price, Integer volumeValue) {
        this(stockSymbolValue, tickDateValue);

        setPrice(price);
        setVolume(volumeValue);
    }

    public StockTick(StockSymbol stockSymbolValue, TickDate tickDateValue, Double price, Integer volumeValue, Double ema13, Double ema26) {
        this(stockSymbolValue, tickDateValue, price, volumeValue);

        setEma13(ema13);
        setEma26(ema26);
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long idValue) {
        id = idValue;
    }

    public StockSymbol getStockSymbol() {
        return stockSymbol;
    }
    @NotNull
    public void setStockSymbol( final StockSymbol stockSymbolValue) {
        stockSymbol = stockSymbolValue;
        stockSymbol.getStockTicks().add(this);
    }

    public Integer getVolume() {
        return volume;
    }
    @NotNull
    public void setVolume( final Integer volumeValue) {
        volume = volumeValue;
    }

    public String getDate() {
        return getTickDate().getDate();
    }

    public TickDate getTickDate() {
        if (tickDate == null) throw new IllegalStateException("TickDate is null!");
        return tickDate;
    }
    @NotNull
    public void setTickDate( final TickDate tickDateValue) {
        tickDate = tickDateValue;
        tickDate.getTicksForDate().add(this);
    }



    public Double getPrice() {
    	return (price instanceof BigDecimal) ? price.doubleValue() : Double.NaN;
    }

    public void setPrice(final BigDecimal priceValue) {
        price = priceValue;
    }

    public void setPrice(final Double priceValue) {
        BigDecimal bigDecimal = new BigDecimal(priceValue, MathContext.DECIMAL32);
        bigDecimal.setScale(2, RoundingMode.HALF_EVEN);
        setPrice(bigDecimal);
    }

    public void setEma13(final Double ema13Value) {
        BigDecimal bigDecimal = new BigDecimal(ema13Value, MathContext.DECIMAL32);
        bigDecimal.setScale(3, RoundingMode.HALF_EVEN);
        setEma13(bigDecimal);
    }

    public void setEma26(final Double ema26Value) {
        BigDecimal bigDecimal = new BigDecimal(ema26Value, MathContext.DECIMAL32);
        bigDecimal.setScale(3, RoundingMode.HALF_EVEN);
        setEma26(bigDecimal);
    }

    public Double getEma13() {
    	return (ema13 instanceof BigDecimal) ? ema13.doubleValue() : Double.NaN;
    }

    private void setEma13(final BigDecimal ema13Value) {
        ema13 = ema13Value;
    }

    public Double getEma26() {
        return (ema26 instanceof BigDecimal) ? ema26.doubleValue() : Double.NaN;
    }

    private void setEma26(final BigDecimal ema26Value) {
        ema26 = ema26Value;
    }

    @Override
    public int compareTo(final LocalDate otherDate) {
        return this.getTickDate().getTickDateLD().compareTo(otherDate);
    }


}
