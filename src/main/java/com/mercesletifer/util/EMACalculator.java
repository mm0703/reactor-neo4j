package com.mercesletifer.util;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.sort;

/**
 * @author Michael McConnell
 * @since 20161130
 */
public class EMACalculator {
    static final List<com.mercesletifer.domain.StockTick> ticks = new ArrayList<>();
    static final Double[] ema26Exp = new Double[26];
    static final String StockTickPath = "/home/michael/Downloads/10-year-stock-prices";
    static Double[] ema13Exp = new Double[13];

    public static void main(String argv[]) {
        EMACalculator emaCalc = new EMACalculator();
        double[] ema10Array = emaCalc.calculateEMA(8, TestPriceData.GOOG);
        double[] ema15Array = emaCalc.calculateEMA(13, TestPriceData.GOOG);
        double[] ema20Array = emaCalc.calculateEMA(26, TestPriceData.GOOG);
        for (int inx = 0; inx < ema10Array.length; inx++){
            System.out.println(TestPriceData.GOOG[inx]+"|"+ema10Array[inx]+"|"+ema15Array[inx]+"|"+ema20Array[inx]);
        }
    }
    private List<com.mercesletifer.domain.StockTick> stockTicks;

    public EMACalculator() {}

    /**
     * Calculates the ema for a given array of prices
     * @param windowLength
     * @param priceArray
     * @return
     */
    public double[] calculateEMA(int windowLength, double[] priceArray) {

		/*
         * Initialize array to original closing prices
		 */

        double[] emaArray = new double[priceArray.length];
        int inx = 0;

        for (inx = 0; inx < priceArray.length; inx++) {
            int prevInx = inx - 1 > 0 ? inx - 1 : 0;
            double prevEma = emaArray[prevInx];
            emaArray[inx] = ema(windowLength, priceArray[inx], prevEma);
        }
        return emaArray;
    }

    public List<com.mercesletifer.domain.StockTick> calculateMACD(List<com.mercesletifer.domain.StockTick> stockTicks) {
        com.mercesletifer.domain.StockTick[] sortedAscending = stockTicks.toArray(new com.mercesletifer.domain.StockTick[stockTicks.size()]);
        sort(sortedAscending, (com.mercesletifer.domain.StockTick p1, com.mercesletifer.domain.StockTick p2) -> {
            if (p1 == null) throw new IllegalArgumentException("StockTick paramter 1 is null!");
            if (p2 == null)  throw new IllegalArgumentException("StockTick paramter 2 is null!");
            return p1.getTickDate().getTickDateLD()
                    .compareTo(p2.getTickDate().getTickDateLD());
        });
        double[] prices = new double[stockTicks.size()];
        double[] ema13 = new double[stockTicks.size()];
        double[] ema26 = new double[stockTicks.size()];

        /*
		 * Initialize price array to closing prices
		 */
        int inx = 0;
        for (com.mercesletifer.domain.StockTick stockTick : sortedAscending) {
            prices[inx++] = stockTick.getPrice();
        }
        ema13 = calculateEMA(13, prices);
        ema26 = calculateEMA(26, prices);
        stockTicks.clear();
        for (inx = 0; inx < prices.length; inx++){
            sortedAscending[inx].setEma13(ema13[inx]);
            sortedAscending[inx].setEma26(ema26[inx]);
            stockTicks.add(sortedAscending[inx]);
        }
        return stockTicks;
    }
    /**
     * Constructs a exponential moving average for current price
     * <p>
     * EMA[today] = (Price[today] x K) + (EMA[yesterday] x (1 – K))
     * <p>
     * where t = today(index in time series), k = 2/(filterLength + 1), y =
     * Price(yesterday)
     *
     * @param filterLength
     * @param priceToday
     * @param emaYesterday
     * @return
     */
    public double ema(int filterLength, double priceToday, double emaYesterday) {
        double ema = 0;
        double k = 2.0 / (filterLength + 1);
        ema = (priceToday * k)  + emaYesterday * (1.0 - k);
//		LOG.debug("filterLength = {}, priceToday = {}, emaYesterday = {}, k = {}, (priceToday - emaYesterday) = {}, (priceToday - emaYesterday) * k = {}, ema = {}",filterLength, priceToday, emaYesterday, k, (priceToday-emaYesterday),(priceToday - emaYesterday) * k, ema);
        return ema;
    }

}
