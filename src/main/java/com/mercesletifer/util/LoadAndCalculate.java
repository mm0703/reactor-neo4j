package com.mercesletifer.util;

import com.mercesletifer.domain.StockSymbol;
import com.mercesletifer.domain.StockTick;
import com.mercesletifer.domain.TickDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.*;

/**
 * @author Michael McConnell
 * @since 20161130
 */
public class LoadAndCalculate {
    static final Logger LOG = LoggerFactory.getLogger(LoadAndCalculate.class);


    static final String StockTickPath = "/home/michael/Downloads/10-year-stock-prices/";
    static Map<StockSymbol, List<StockTick>> Symbol2TickMap;
    static List<StockSymbol> SymbolList;
    static double[] ema13Exp;
    static double[] ema26Exp;
    static List<File> fileList;
    static File StockDir = null;
    static Map<String, List<String>> NameToCSVListMap;
    static Map<LocalDate, TickDate> LocalDate2TickDateMap;
    private static Map<String, File> Name2CSVFileMap;
    /**
     * Constructs a exponential moving average  for current price
     * EMA = Price(t) * k + EMA(y) * (1 - k)
     * where t = today(index in time series), k = 2/(filterLength + 1), y = Price(yesterday)
     * @param filterLength
     * @paran priceToday
     * @param emaYesterday
     * @return
     */
    public static double ema(int filterLength, double priceToday, double emaYesterday) {
        double k = 2.0 / (filterLength + 1);
        return (priceToday * k) + (emaYesterday * (1.0 - k));
    }


    private static List<StockTick> CalculateEMA13(List<StockTick> stockTimeSeries) {

        StockTick[] sortedAscending = stockTimeSeries.toArray(new StockTick[stockTimeSeries.size()]);
        Arrays.sort(sortedAscending,
                new Comparator<StockTick>() {
                    public int compare(final StockTick st1, final StockTick st2) {
                        return st1.getTickDate().getTickDateLD().compareTo(st2.getTickDate().getTickDateLD());
                    }
                });
        double[] ema13Array = new double[stockTimeSeries.size()];
        double[] prices = new double[stockTimeSeries.size()];
        int inx = 0;

        int windowSize = 13;
        /*
         * Initialize array to original closing prices
         */
        for (StockTick stockTick : stockTimeSeries) {
            prices[inx++] = stockTick.getPrice();
        }
        for (inx = 0; inx < prices.length; inx++){
            int prevInx = inx > 0 ? inx - 1 : 0;
            double prevEma = ema13Array[prevInx];
            ema13Array[inx] = ema(windowSize, prices[inx], prevEma);
            LOG.debug("inx = {}, prices[{}] = {}, ema13Array[{}] = {}", inx,inx, prices[inx], inx, ema13Array[inx]);
        }
        for (int i$ = 0; i$ < ema13Array.length; i$++) {
            sortedAscending[i$].setEma13(ema13Array[i$]);
        }
        return stockTimeSeries;
    }

    private static List<StockTick> CalculateEMA26(List<StockTick> stockTimeSeries) {
        StockTick[] sortedAscending = stockTimeSeries.toArray(new StockTick[stockTimeSeries.size()]);
        Arrays.sort(sortedAscending,
                new Comparator<StockTick>() {
                    public int compare(final StockTick st1, final StockTick st2) {
                        return st1.getTickDate().getTickDateLD().compareTo(st2.getTickDate().getTickDateLD());
                    }
                });
        /*
         * Initialize array to original closing prices
         */
        int windowSize = 26;
        double[] prices = new double[stockTimeSeries.size()];
        double[] ema26Array = new double[stockTimeSeries.size()];
        int inx = 0;
        for (StockTick stockTick : stockTimeSeries) {
            prices[inx++] = stockTick.getPrice();
        }
        for (inx = 0; inx < prices.length; inx++) {
            int prevInx = inx > 0 ? inx - 1 : 0;
            double prevEma = ema26Array[prevInx];
            ema26Array[inx] = ema(windowSize, prices[inx], ema(windowSize, prices[prevInx], prevEma));
            LOG.debug("inx = {}, prices[{}] = {}, ema26Array[{}] = {}", inx, inx, prices[inx], inx, ema26Array[inx]);
        }
        for (int i$ = 0; i$ < ema26Array.length; i$++) {
            sortedAscending[i$].setEma26(ema26Array[i$]);
        }
        return stockTimeSeries;
    }

    private static Map<String, List<String>> GetName2CSVListMap() {
        if (NameToCSVListMap instanceof Map) {
            return NameToCSVListMap;
        }
        NameToCSVListMap = new HashMap<>();
        Set<String> fileNames = GetName2CSVFileMap().keySet();
        for (String fName : fileNames) {
            File csvFile = GetName2CSVFileMap().get(fName);
            try {
                BufferedReader bReader = new BufferedReader(new FileReader(csvFile));
                String buff = null;
                List<String> csvlist = new ArrayList<>();
                while ((buff = bReader.readLine()) != null) {
                    csvlist.add(buff);
                }
                NameToCSVListMap.put(fName, csvlist);
            } catch (Throwable t) {
                LOG.error("unexpected error when reading csv file: {}", fName, t);
            }

        }
        return NameToCSVListMap;
    }

    private static Map<String, File> GetName2CSVFileMap() {
        if (Name2CSVFileMap == null) {
            Name2CSVFileMap = InitializeInput();
        }
        return Name2CSVFileMap;
    }


    private static Map<LocalDate, TickDate> GetLocalDate2TickDateMap() {
        if (LocalDate2TickDateMap == null) LocalDate2TickDateMap = new HashMap<>();
        return LocalDate2TickDateMap;
    }

    public static List<StockSymbol> GetStockTicks() {
        if (SymbolList instanceof List && !SymbolList.isEmpty()) {
            return SymbolList;
        }
        SymbolList = new ArrayList<>();

        for (String name : GetName2CSVListMap().keySet()) {
            StockSymbol sSymbol = new StockSymbol(name);
            int lineCount = 0;
            for (String csValues : GetName2CSVListMap().get(name)) {
                if (lineCount++ > 0) {

                    String[] tokenArray = csValues.split(",");
                    //Date

                    TickDate tickDate = new TickDate(tokenArray[0]);
                    if (GetLocalDate2TickDateMap().get(tickDate.getTickDateLD()) != null) {
                        tickDate = GetLocalDate2TickDateMap().get(tickDate.getTickDateLD());
                    } else {
                        GetLocalDate2TickDateMap().put(tickDate.getTickDateLD(), tickDate);
                    }
                    StockTick tick = new StockTick(sSymbol, tickDate);
//                    LOG.debug("Adding tick for symbol: {}, with tick date: {}", sSymbol.getSymbol(), tick.getTickDate().getDate());
                    //Open
//                    if (tokenArray[1].equals("-")) tokenArray[1] = "0";
//                    tick.setOpen(Double.valueOf(tokenArray[1]));
//                    //High
//                    if (tokenArray[2].equals("-")) tokenArray[2] = "0";
//                    tick.setHi(Double.valueOf(tokenArray[2]));
//                    // Low
//                    if (tokenArray[3].equals("-")) tokenArray[3] = "0";
//                    tick.setLow(Double.valueOf(tokenArray[3]));
                    // Close
                    try {
                        tick.setPrice(Double.valueOf(tokenArray[4]));
                    } catch (NumberFormatException nfe) {
                        LOG.error("error with stock: {}, tick close price {}", sSymbol.getSymbol(), tokenArray[4], nfe);
                        continue;
                    }
                    // Volume
                    if (tokenArray[5].equals("-")) tokenArray[5] = "0";
                    tick.setVolume(Integer.valueOf(tokenArray[5]));
                }
            }
            SymbolList.add(sSymbol);
        }
        for (StockSymbol symbol : SymbolList) {
            LOG.debug("processing ema13 for symbol: {}, with {} stockTicks",symbol.getSymbol(), symbol.getStockTicks().size());
            CalculateEMA13( symbol.getStockTicks());
            LOG.debug("processing ema26 for symbol: {}, with {} stockTicks",symbol.getSymbol(), symbol.getStockTicks().size());
            CalculateEMA26(symbol.getStockTicks());
        }
        return SymbolList;
    }


    private static Map<String, File> InitializeInput() {
        Map<String, File> name2CSVFileMap = new HashMap<>();
        try {
            StockDir = new File(StockTickPath);
            String[] stockFiles = StockDir.list();
            for (String fname : stockFiles) {
                File csvFile = new File(StockTickPath + fname);
                String symName = csvFile.getName();
                int start, end;
                start = 0;
                end = symName.indexOf(".csv");
                CharSequence symCharSeq = symName.subSequence(start, end);
                if (csvFile.exists() && csvFile.getCanonicalPath().endsWith(".csv")) {
                    name2CSVFileMap.put(symCharSeq.toString(), csvFile);
                    LOG.debug("name2CSVFileMap adding symbol: {}, with file: {}", symCharSeq.toString(), csvFile.getCanonicalPath());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name2CSVFileMap;
    }


    public static void main(String argv[]) {

        GetStockTicks();

    }


}
