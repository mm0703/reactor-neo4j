package com.mercesletifer.common;


import java.io.Serializable;

/**
 * @author Michael McConnell
 * @since 20161201
 */
public class InitializationException extends RuntimeException implements Serializable {
    private static final long serialVersionUID = 8903835544133330659L;

    public InitializationException() {
        super();
    }

    public InitializationException(final String message) {
        super(message);
    }

    public InitializationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InitializationException(final Throwable cause) {
        super(cause);
    }

    protected InitializationException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
