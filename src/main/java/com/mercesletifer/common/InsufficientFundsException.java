package com.mercesletifer.common;

import java.io.Serializable;

/**
 * @author Michael McConnell
 * @since 20161216
 */
public class InsufficientFundsException extends RuntimeException implements Serializable{
    private static final long serialVersionUID = 5395362768458938502L;

    public InsufficientFundsException() {
    }

    public InsufficientFundsException( final String message) {
        super(message);
    }

    public InsufficientFundsException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InsufficientFundsException(final Throwable cause) {
        super(cause);
    }

    public InsufficientFundsException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
