package com.mercesletifer.common;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

public class PasswordHasher {
    private ShaPasswordEncoder encoder;
    private String salt;

    public PasswordHasher(ShaPasswordEncoder encoder, String salt) {
        this.encoder = encoder;
        this.salt = salt;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public ShaPasswordEncoder getEncoder() {
        return encoder;
    }

    public void setEncoder(ShaPasswordEncoder encoder) {
        this.encoder = encoder;
    }
}
