package com.mercesletifer.common;

/**
 * @author Michael McConnell
 * @since 20161215
 */
public class ExportConversionException extends RuntimeException {
    public ExportConversionException() {
    }

    public ExportConversionException(final String message) {
        super(message);
    }

    public ExportConversionException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ExportConversionException(final Throwable cause) {
        super(cause);
    }

    public ExportConversionException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
